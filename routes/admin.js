var express = require('express');
var router = express.Router();
var multer  = require('multer');
var Common = require('../models/common');
var Article = require('../models/article');
var upload = multer({ dest: 'uploads/' });
var importDataService = require('../services/import-data-service.js');

/* GET home page. */
router.post('/upload', upload.single('dataFile'), function(req, res, next) {
  importDataService.importData(req.file.path, function() {
    res.sendStatus(200);
  });
});

router.post('/uploadEod', upload.single('dataEodFile'), function(req, res, next) {
  console.log(req.file.path);
  importDataService.importEodData(req.file.path, function() {
    res.sendStatus(200);
  });
});

router.get('/latestUpdate', function(req, res, next) {
  Common.findOne({}, function(err, common) {
    res.send(common);
  })
});

router.get('/allArticles', function(req, res, next) {
  Article.find({}, function(err, article) {
    res.send(article);
  })
});

router.post('/addArticle', function(req, res, next) {
  var data = req.body;
  data.day = new Date();
    Article.collection.insert(data, function(err, article) {
        res.send(article);
    });
});


module.exports = router;
