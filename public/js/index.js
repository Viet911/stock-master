angular.module('index', ['ngRoute']);

angular.module('index').config(function($routeProvider) {
    $routeProvider
        .when('/', {
            controller: 'IndexController',
            templateUrl: '/html/index/index.html'
        }).when('/detail/:ticker', {
            controller: 'DetailController',
            templateUrl: '/html/index/detail.html'
        })
        .otherwise({
            redirectTo: '/'
        });
});