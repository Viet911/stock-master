var app = angular.module('admin', ['ui.bootstrap', 'ngFileUpload', 'ngRoute', 'ngAnimate', 'colorpicker.module', 'wysiwyg.module']);

angular.module('admin').config(function($routeProvider) {
    $routeProvider
        .when('/upload', {
            controller: 'UploadFileController',
            templateUrl: '/html/admin/upload.html'
        }).when('/news', {
            controller: 'ArticlesManagementController',
            templateUrl: '/html/admin/articles.html'
        })
        .otherwise({
            redirectTo: '/upload'
        });
});

app.service('CommonService', ['$http', function($http) {
    return {
        getLatestUpdate: function(cb) {
            $http({
                method: 'GET',
                url: '/admin/latestUpdate'
            }).then(function successCallback(response) {
                cb(null, response.data.latestUpdate);
            }, function errorCallback(response) {});
        },
        getAllArticles: function(cb) {
            $http({
                method: 'GET',
                url: '/admin/allArticles'
            }).then(function successCallback(response) {
                cb(null, response.data);
            }, function errorCallback(response) {});
        }
    };
}]);

app.controller('UploadFileController', ['$scope', 'Upload', 'CommonService', function($scope, Upload, CommonService) {
    function loadLastestUpdate() {
        CommonService.getLatestUpdate(function(err, latestUpdate) {
            $scope.latestUpdate = latestUpdate;
        });
    }

    $scope.upload = function() {
        Upload.upload({
            url: "/admin/upload",
            data: {
                dataFile: $scope.dataFile
            }
        }).then(function(res) {
            alert('Data Updated Successfully!');
            loadLastestUpdate();
        });
    };

    $scope.uploadEod = function() {

        Upload.upload({
            url: "/admin/uploadEod",
            data: {
                dataEodFile: $scope.dataEodFile
            }
        }).then(function(res) {
            alert('Eod Updated Successfully!');
            loadLastestUpdate();
        });
    };

    loadLastestUpdate();
}]);

app.controller('ArticlesManagementController', ['$scope', 'CommonService', '$uibModal', function($scope, CommonService, $uibModal) {
    function loadArticles() {
        CommonService.getAllArticles(function(err, articles) {
            console.log(articles);
            $scope.articlesList = articles;
        });
    }

    $scope.open = function(flag) {
        console.log(flag);
        if (flag) {
            $scope.isCreate = true;
        } else {
            $scope.isCreate = false;
        }
        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: 'myModalContent.html',
            controller: 'EditModalCtrl',
            size: 'lg',
            resolve: {
                id: function() {

                },
                isCreate: function() {
                    return $scope.isCreate;
                }
            }
        });

        modalInstance.result.then(function() {}, function() {
            loadArticles();
        });
    };

    $scope.checkAll = function () {
        if ($scope.selectedAll) {
            $scope.selectedAll = true;
        } else {
            $scope.selectedAll = false;
        }
        angular.forEach($scope.articles, function (item) {
            item.Selected = $scope.selectedAll;
        });

    };

    $scope.edit = function() {
        $scope.open(false);
    }

    loadArticles();
}]);

app.controller('EditModalCtrl', ['$scope', '$uibModalInstance', '$http', 'isCreate', function($scope, $uibModalInstance, $http, isCreate) {

    $scope.disabled = false;
    $scope.isCreate = isCreate;
    $scope.menu = [
        ['bold', 'italic', 'underline', 'strikethrough', 'subscript', 'superscript'],
        ['format-block'],
        ['font'],
        ['font-size'],
        ['font-color', 'hilite-color'],
        ['remove-format'],
        ['ordered-list', 'unordered-list', 'outdent', 'indent'],
        ['left-justify', 'center-justify', 'right-justify'],
        ['code', 'quote', 'paragraph'],
        ['link', 'image'],
        ['css-class']
    ];

    $scope.setDisabled = function() {
        $scope.disabled = !$scope.disabled;
    };

    $scope.save = function() {
        $http.post('/admin/addArticle', $scope.data).then(function(data) {}, function(err) {});
        $uibModalInstance.close();
    };

    $scope.cancel = function() {
        $uibModalInstance.dismiss('cancel');
    };
}]);
