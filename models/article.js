var mongoose = require('mongoose'),
  Schema = mongoose.Schema;

var ArticleSchema = new Schema({
  title: String,
  day: Date,
  content: String,
  tag: []
});

module.exports = mongoose.model('Article', ArticleSchema);