var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var TickerSchema = new Schema ({
  ticker: String,
  industry: String,
  PE: String,
  EPSGrowth: String,
  ROE: String,
  SMGRating: String 
})

module.exports = mongoose.model('Ticker', TickerSchema);