var mongoose = require('mongoose'),
  Schema = mongoose.Schema;

var CommonSchema = new Schema({
  latestUpdate: {
      type: Date
    }
});

module.exports = mongoose.model('Common', CommonSchema);