var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var SignalSchema = new Schema({
  ticker: String,
  day: Date,
  price: Number,
  volume: Number,
  OneMonthTrend: String,
  ThreeMonthTrend: String,
  signal: String,
  points: Number,
  noise: Number
})

module.exports = mongoose.model('Signal', SignalSchema);