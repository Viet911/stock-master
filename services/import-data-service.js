var XLSX = require('xlsx');
var dateUtil = require('../utils/date-util');
var Ticker = require('../models/ticker');
var Signal = require('../models/signal');
var Common = require('../models/common');
var Eod = require('../models/eod');
var readline = require('readline');
var fs = require('fs');

module.exports = {
    importData: function(fileName, callback) {
        var workbook = XLSX.readFile(fileName);
        var data = workbook.Sheets[workbook.SheetNames[4]];

        var cell = ""
        var row = 5;
        var column = "A";
        var lastTicker = null;
        var signals = [];
        var tickers = [];
        var ticker = null;

        do {
            ticker = data["A" + row];
            if (!ticker) {
                break;
            };

            ticker = ticker.v;

            signals.push({
                ticker: ticker,
                day: typeof(data["B" + row]) !== 'undefined' ? dateUtil.parseDate(String(data["B" + row].v)) : "NONE",
                price: typeof(data["I" + row]) !== 'undefined' ? data["I" + row].v : "NONE",
                volume: typeof(data["K" + row]) !== 'undefined' ? data["K" + row].v : "NONE",
                OneMonthTrend: typeof(data["M" + row]) !== 'undefined' ? data["M" + row].v : "NONE",
                ThreeMonthTrend: typeof(data["N" + row]) !== 'undefined' ? data["N" + row].v : "NONE",
                signal: typeof(data["O" + row]) !== 'undefined' ? data["O" + row].v : "NONE",
                points: typeof(data["P" + row]) !== 'undefined' ? data["P" + row].v : "NONE",
                noise: typeof(data["Q" + row]) !== 'undefined' ? data["Q" + row].v : "NONE"
            })
            if (ticker != lastTicker) {
                tickers.push({
                    ticker: ticker,
                    industry: typeof(data["C" + row]) !== 'undefined' ? data["C" + row].v : "NONE",
                    PE: typeof(data["F" + row]) !== 'undefined' ? data["F" + row].v : "NONE",
                    EPSGrowth: typeof(data["E" + row]) !== 'undefined' ? data["E" + row].v : "NONE",
                    ROE: typeof(data["G" + row]) !== 'undefined' ? data["G" + row].v : "NONE",
                    SMGRating: typeof(data["H" + row]) !== 'undefined' ? data["H" + row].v : "NONE"
                })
            }
            lastTicker = ticker;
            cell = column + row;
            row++;
        } while (data[cell]);
        Ticker.collection.insert(tickers, function() {
            console.log('Done writing Ticker');
            Signal.collection.insert(signals, function() {
                console.log('Done writing Signal');
                Signal.findOne({}).sort('-day').exec(function(err, eod) {
                    Common.findOne({}, function(err, common) {
                        if (!common) {
                            Common.collection.insert([{ latestUpdate: eod.day }], function() {
                                console.log('Done create new common');
                                callback();
                            })
                        } else {
                            common.latestUpdate = eod.day;
                            common.save(callback);
                            console.log('Done update new common');
                        }
                    });
                })
            });
        });
    },
    importEodData: function(fileName, callback) {
        var rl = readline.createInterface({
            input: fs.createReadStream(fileName)
        });
        var data = [];
        rl.on('line', function(line) {
            var str = line;
            var arr = str.split(',');
            //fet raw data to mongodb
            if (arr[0] !== "<Ticker>") {
                data.push({
                    ticker: arr[0],
                    day: dateUtil.parseDate(arr[1]),
                    open: parseFloat(arr[2]),
                    high: parseFloat(arr[3]),
                    low: parseFloat(arr[4]),
                    close: parseFloat(arr[5]),
                    volume: parseFloat(arr[6]),
                });
            }
        });
        rl.on('close', function() {
            console.log('Done reading');
            Eod.collection.insert(data, function() {
                Eod.findOne({}).sort('-day').exec(function(err, eod) {
                    Common.findOne({}, function(err, common) {
                        if (!common) {
                            Common.collection.insert([{ latestUpdate: eod.day }], function() {
                                console.log('Done create new common');
                                callback();
                            })
                        } else {
                            common.latestUpdate = eod.day;
                            common.save(callback);
                            console.log('Done update new common');
                        }
                    });
                })
            });
        });
    }
}
